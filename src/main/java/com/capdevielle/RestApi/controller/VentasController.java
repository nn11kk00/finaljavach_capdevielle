package com.capdevielle.RestApi.controller;


import com.capdevielle.RestApi.exception.ResourceNotFoundException;
import com.capdevielle.RestApi.model.Venta;
import com.capdevielle.RestApi.service.VentasService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping(path = "api/comprobante")
@RestController
public class VentasController {
    @Autowired
    private VentasService vs;

    @GetMapping("/")
    public ResponseEntity<List<Venta>> getAllVentas() {
        List<Venta> ventas = vs.getAllVentas();
        return new ResponseEntity<>(ventas, HttpStatus.OK);
    }



    @PostMapping("/")
    public ResponseEntity<Venta> create (@RequestBody Venta v) throws ResourceNotFoundException {
        return new ResponseEntity<>(this.vs.create(v), HttpStatus.OK);
    }

}
