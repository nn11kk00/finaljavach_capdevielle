package com.capdevielle.RestApi;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.init.ScriptUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

@SpringBootApplication
public class RestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestApiApplication.class, args);
		System.out.println("Corriendo Aplicacion");
	}

	@Bean
	public CommandLineRunner initData(DataSource dataSource) {
		return args -> {
			try (Connection connection = dataSource.getConnection()) {
				ClassPathResource resource = new ClassPathResource("script/data.sql");
				ScriptUtils.executeSqlScript(connection, resource);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		};
	}
}
