-- Crear las tablas
CREATE TABLE IF NOT EXISTS clientes (
  id INT AUTO_INCREMENT PRIMARY KEY,
  dni VARCHAR(11),
  nombre VARCHAR(100),
  apellido VARCHAR(45),
  fecha_nacimiento DATE
);

CREATE TABLE IF NOT EXISTS productos (
  id INT AUTO_INCREMENT PRIMARY KEY,
  descripcion VARCHAR(150),
  precio_compra DOUBLE,
  precio_venta DOUBLE,
  stock INT,
  fecha_alta DATE
);

CREATE TABLE IF NOT EXISTS ventas (
  id INT AUTO_INCREMENT PRIMARY KEY,
  fecha_alta DATE,
  total DOUBLE,
  cliente_id INT,
  CONSTRAINT fk_ventas_clientes FOREIGN KEY (cliente_id) REFERENCES clientes(id)
);

CREATE TABLE IF NOT EXISTS detalles_venta (
  id INT AUTO_INCREMENT PRIMARY KEY,
  venta_id INT NOT NULL,
  producto_id INT NOT NULL,
  cantidad INT,
  sub_total DOUBLE,
  CONSTRAINT fk_detalles_venta_ventas FOREIGN KEY (venta_id) REFERENCES ventas(id),
  CONSTRAINT fk_detalles_venta_productos FOREIGN KEY (producto_id) REFERENCES productos(id)
);
